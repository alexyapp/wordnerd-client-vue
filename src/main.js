import Vue from 'vue';
import App from './App.vue';
import store from './store';
import vuetify from './plugins/vuetify';
import router from './router';
import './utils/validation';
import './plugins/bootstrap-vue';
import Default from './components/Layouts/Default';
import NoNavbar from './components/Layouts/NoNavbar';

Vue.config.productionTip = false;

Vue.component('default-layout', Default);
Vue.component('no-navbar-layout', NoNavbar);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
