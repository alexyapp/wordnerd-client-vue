import axios from 'axios';

const httpClient = axios.create({
    baseURL: process.env.VUE_APP_BASE_URL,
    timeout: 5000,
    headers: {
        "Content-Type": "application/json"
    }
});

httpClient.interceptors.request.use(function(config) {
    const token = localStorage.getItem('token');
    config.headers.Authorization =  token ? `Bearer ${token}` : '';
    return config;
});

httpClient.interceptors.response.use(function(response) {
    return response;
}, function (error) {
    const originalRequest = error.config;

    if (error.response && error.response.status === 401) {
        // refresh token
    }

    throw error;
});


export default httpClient;