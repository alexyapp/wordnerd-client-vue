import httpClient from './httpClient';

const getCurrentUserData = () => httpClient.get('/auth/me');

const login = (email, password) => httpClient.post('/auth/login', { email, password });

const register = (name, email, password, password_confirmation, dob, username) => httpClient.post('/auth/register', { name, email, password, password_confirmation, dob, username });

const logout = () => httpClient.post('/auth/logout');

export {
    getCurrentUserData,
    login,
    register,
    logout
}