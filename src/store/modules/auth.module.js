import * as api from '@/api/auth.api';
import { 
    LOGIN_REQUEST,
    LOGIN_SUCCESS, 
    LOGIN_ERROR,
    REGISTER_REQUEST,
    REGISTER_SUCCESS, 
    REGISTER_ERROR,
    GET_CURRENT_USER_DATA_SUCCESS, 
    GET_CURRENT_USER_DATA_ERROR,
    LOGOUT_REQUEST,
    LOGOUT_SUCCESS, 
    LOGOUT_ERROR
} from '../mutation-types';

const state = {
    token: '',
    user: JSON.parse(localStorage.getItem('user')) || {},
    loading: false
}

const getters = {
    
}

const actions = {
    async getCurrentUserData({ commit }) {
        try {
            const { data: { data: user } } = await api.getCurrentUserData();

            commit(GET_CURRENT_USER_DATA_SUCCESS, user);
        } catch (error) {
            commit(GET_CURRENT_USER_DATA_ERROR);
        }
    },

    login({ commit, dispatch }, payload) {
        return new Promise((resolve, reject) => {
            commit(LOGIN_REQUEST);

            const { email, password } = payload;
            
            api.login(email, password)
                .then(response => {
                    const { data: { access_token } } = response;

                    commit(LOGIN_SUCCESS, access_token);

                    dispatch('getCurrentUserData');

                    resolve(response);
                })
                .catch(error => {
                    commit(LOGIN_ERROR);
                    reject(error);
                });
        });
    },

    register({ commit, dispatch }, payload) {
        return new Promise((resolve, reject) => {
            commit(REGISTER_REQUEST);

            const { name, email, password, password_confirmation, dob, username } = payload;

            api.register(name, email, password, password_confirmation, dob, username)
                .then(response => {
                    const { data: { access_token } } = response;

                    commit(REGISTER_SUCCESS, access_token);

                    dispatch('getCurrentUserData');

                    resolve(response);
                })
                .catch(error => {
                    commit(REGISTER_ERROR);
                    reject(error);
                });
        });
    },

    logout({ commit }) {
        return new Promise((resolve, reject) => {
            commit(LOGOUT_REQUEST);

            api.logout()
                .then(response => {
                    commit(LOGOUT_SUCCESS);

                    resolve(response);
                })
                .catch(error => {
                    commit(LOGOUT_ERROR);
                    reject(error);
                });
        });
    }
}

const mutations = {
    [GET_CURRENT_USER_DATA_SUCCESS](state, user) {
        state.user = user;
        state.loading = false;
        localStorage.setItem('user', JSON.stringify(user));
    },

    [GET_CURRENT_USER_DATA_ERROR](state) {
        state.user = {};
        state.loading = false;
        localStorage.removeItem('user');
    },

    [LOGIN_REQUEST](state) {
        state.loading = true;
    },

    [LOGIN_SUCCESS](state, token) {
        state.token = token;
        localStorage.setItem('token', token);
    },
    
    [LOGIN_ERROR](state) {
        state.token = '';
        state.loading = false;
        localStorage.removeItem('token');
    },

    [REGISTER_REQUEST](state) {
        state.loading = true;
    },

    [REGISTER_SUCCESS](state, token) {
        state.token = token;
        localStorage.setItem('token', token);
    },
    
    [REGISTER_ERROR](state) {
        state.token = '';
        state.loading = false;
        localStorage.removeItem('token');
    },

    [LOGOUT_REQUEST](state) {
        state.loading = true;
    },

    [LOGOUT_SUCCESS](state) {
        state.loading = false;
        localStorage.removeItem('token');
        localStorage.removeItem('user');
    },
    
    [LOGOUT_ERROR](state) {
        // TODO: handle error
        state.loading = false;
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}