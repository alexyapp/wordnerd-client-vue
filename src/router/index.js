import Vue from 'vue';
import VueRouter from 'vue-router';

import routes from './routes';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem('token')) {
            // if (to.matched.some(record => record.meta.is_admin)) {
            //     let currentUser = JSON.parse(localStorage.getItem('user'));

            //     // TODO: refactor insecure approach
            //     if (currentUser.roles.includes('admin')) {
            //         next();
            //     }

            //     next({ path: '/home' });
            // }

            next();
        } else {
            next({
                path: '/',
                params: {
                    nextUrl: to.fullPath
                }
            });
        }
    } else if (to.matched.some(record => record.meta.guest)) {
        if (localStorage.getItem('token')) {
            next({ path: '/home' });
        } else {
            next();
        }
    } else {
        next();
    }
});

export default router;