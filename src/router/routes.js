import LandingPage from '@/pages/LandingPage';
import HomePage from '@/pages/HomePage';
import LoginPage from '@/pages/LoginPage';
import RegisterPage from '@/pages/RegisterPage';

const routes = [
    {
        name: 'landing',
        path: '/',
        component: LandingPage,
        meta: {
            guest: true,
            layout: 'no-navbar'
        }
    },
    {
        name: 'home',
        path: '/home',
        component: HomePage,
        meta: {
            requiresAuth: true,
        }
    },
    {
        name: 'login',
        path: '/login',
        component: LoginPage,
        meta: {
            guest: true,
            layout: 'no-navbar'
        }
    },
    {
        name: 'register',
        path: '/register',
        component: RegisterPage,
        meta: {
            guest: true,
            layout: 'no-navbar'
        }
    }
];

export default routes;