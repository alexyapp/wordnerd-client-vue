import { extend } from 'vee-validate';
import { required, email, alpha_dash } from 'vee-validate/dist/rules';

// extend('required', {
//     validate(value) {
//         return {
//             required: true,
//             valid: ['', null, undefined].indexOf(value) === -1
//         };
//     },
//     computesRequired: true
// });

extend('required', {
    ...required,
    message: 'The {_field_} field is required.'
});

extend('email', {
    ...email,
    message: "That doesn't look to be like an email..."
});

extend('alpha_dash', alpha_dash);

extend('min', {
    validate(value, args) {
        return value.length >= args.length;
    },
    params: ['length'],
    message: 'The {_field_} field must consist of at least {length} characters.'
});

extend('minmax', {
    validate(value, { min, max }) {
        return value.length >= min && value.length <= max;
    },
    params: ['min', 'max'],
    message: 'The {_field_} field must consist of at least {min} characters and not more than {max} characters.'
});

extend('one_of', (value, values) => values.indexOf(value) !== -1);